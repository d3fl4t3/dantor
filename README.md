Authenticated dante + tor proxy.

Usage: 
 - `cp shadow.example shadow`
 - `docker-compose up -d`

`shadow` is a unix shadow file used to specify proxy credentials. To create password hash you can use python:
```python
import crypt
print(crypt.crypt("my_password"))
```

Default listen address is 0.0.0.0:25522, you can change it in `docker-compose.yml`
